/*
 * Copyright (c) 2021 LAAS-CNRS
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 2.1 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGLPV2.1
 */

/**
 * @brief   This file it the main entry point of the
 *          OwnTech Power API. Please check the README.md
 *          file at the root of this project for basic
 *          information on how to use the Power API,
 *          or refer the the wiki for detailed information.
 *          Wiki: https://gitlab.laas.fr/owntech/power-api/core/-/wikis/home
 *
 * @author  Clément Foucher <clement.foucher@laas.fr>
 */

//-------------OWNTECH DRIVERS-------------------
#include "HardwareConfiguration.h"
#include "DataAcquisition.h"
#include "Scheduling.h"
#include "power.h"
#include "GpioApi.h"

//------------ZEPHYR DRIVERS----------------------
#include "zephyr/console/console.h"

//------------DEFINITIONS-------------------------
#define LEG1_CAPA_DGND PC6
#define LEG2_CAPA_DGND PB7

//--------------SETUP FUNCTIONS DECLARATION-------------------
void setup_hardware(); //setups the hardware peripherals of the system
void setup_software(); //setups the scheduling of the software and the control method

//-------------LOOP FUNCTIONS DECLARATION----------------------
void loop_communication_task(); //code to be executed in the slow communication task
void loop_application_task();   //code to be executed in the fast application task
void loop_control_task();       //code to be executed in real-time at 20kHz

int8_t communication_task_number; //holds the number of the communication task
int8_t application_task_number; //holds the number of the application task


enum serial_interface_menu_mode //LIST OF POSSIBLE MODES FOR THE OWNTECH CONVERTER
{
    IDLEMODE =0,
    BUCKINITMODE,
    BOOSTINITMODE,
    ACINITMODE,
    BUCKMODE,
    BOOSTMODE,
    ACMODE,
    BUCKPAUSEMODE,
    BOOSTPAUSEMODE,
    ACPAUSEMODE
};

uint8_t received_serial_char;
uint8_t mode = IDLEMODE;

//--------------USER VARIABLES DECLARATIONS----------------------

static float32_t duty_cycle = 0.5; //[-] duty cycle (comm task) - only for buck and boost modes
static float32_t duty_cycle_step = 0.05; //[-] duty cycle step (comm task)
static float32_t duty_cycle_ac = 0.5; //[-] duty cycle (comm task) - only for ac mode

static bool pwm_enable = false; //[bool] state of the PWM (ctrl task)
static uint32_t control_task_period = 50; //[us] period of the control task
static uint32_t time = 0; // time variable - multiple of control_task_period (ctrl task)
static uint32_t frequency = 50; //[Hz] frequency of the AC voltage

//---------------------------------------------------------------


//---------------SETUP FUNCTIONS----------------------------------

void setup_hardware()
{
    hwConfig.setBoardVersion(TWIST_v_1_1_2);
    power.setShieldVersion(shield_TWIST_V1_3);
    console_init();         // initialize the UART console
    dataAcquisition.enableTwistDefaultChannels();
    power.initAllBuck(VOLTAGE_MODE); // initialize the legs in buck mode, setup the HR timers

    // initialize the GPIO pins to control Q5 and Q6 (electrolytic capacitors)
    gpio.configurePin(LEG1_CAPA_DGND, OUTPUT);
    gpio.configurePin(LEG2_CAPA_DGND, OUTPUT);
}

void setup_software()
{
    application_task_number =  scheduling.defineAsynchronousTask(loop_application_task);
    communication_task_number = scheduling.defineAsynchronousTask(loop_communication_task);
    scheduling.defineUninterruptibleSynchronousTask(&loop_control_task, control_task_period);

    scheduling.startAsynchronousTask(application_task_number);
    scheduling.startAsynchronousTask(communication_task_number);
    scheduling.startUninterruptibleSynchronousTask();
}

//---------------LOOP FUNCTIONS----------------------------------

void loop_communication_task()
{
    while(1) {
    received_serial_char = console_getchar();
    switch (received_serial_char) {
        case 'h':
            //----------SERIAL INTERFACE MENU-----------------------
            printk(" _____________________________________\n");
            printk("|     ------- MENU ---------          |\n");
            printk("|     press b : buck mode             |\n");
            printk("|     press s : serial mode           |\n");
            printk("|     press a : ac mode               |\n");
            printk("|     press t : toggle, pause <-> on  |\n");
            printk("|_____________________________________|\n\n");
            //------------------------------------------------------
            break;
        case 'b':
            if (mode == IDLEMODE) {
                printk("Buck mode\n");
                mode = BUCKINITMODE;
            }
            break;
        case 'c':
            if (mode == IDLEMODE) {
                printk("Boost mode\n");
                mode = BOOSTINITMODE;
            }
            break;
        case 'a':
            if (mode == IDLEMODE) {
                printk("AC mode\n");
                mode = ACINITMODE;
            }
            break;
        case 't':
            if (mode == BUCKMODE) {
                printk("Buck pause mode\n");
                mode = BUCKPAUSEMODE;
            }
            else if (mode == BOOSTMODE) {
                printk("Boost pause mode\n");
                mode = BOOSTPAUSEMODE;
            }
            else if (mode == ACMODE) {
                printk("AC pause mode\n");
                mode = ACPAUSEMODE;
            }
            else if (mode == BUCKPAUSEMODE) {
                printk("Buck mode\n");
                mode = BUCKMODE;
            }
            else if (mode == BOOSTPAUSEMODE) {
                printk("Boost mode\n");
                mode = BOOSTMODE;
            }
            else if (mode == ACPAUSEMODE) {
                printk("AC mode\n");
                mode = ACMODE;
            }
            break;
        case 'u':
            printk("duty cycle UP: %f\n", duty_cycle);
            if (mode == BUCKMODE || mode == BUCKPAUSEMODE || mode == BUCKINITMODE) {
                duty_cycle = duty_cycle + duty_cycle_step;
                if(duty_cycle>1.0) duty_cycle = 1.0;
            }
            else if (mode == BOOSTMODE || mode == BOOSTPAUSEMODE || mode == BOOSTINITMODE || mode == IDLEMODE) {
                duty_cycle = duty_cycle + duty_cycle_step;
                if(duty_cycle>0.5) {
                    duty_cycle = 0.5;
                    printk("duty cycle limited to 0.5 in boost mode\n");
                }
            }
            duty_cycle = duty_cycle + duty_cycle_step;
            if(duty_cycle>1.0) duty_cycle = 1.0;
            break;
        case 'd':
            printk("duty cycle DOWN: %f\n", duty_cycle);
            duty_cycle = duty_cycle - duty_cycle_step;
            if(duty_cycle<0.0) duty_cycle = 0.0;
            break;

        default:
            break;
    }}
}


void loop_application_task()
{
    while(1){
        if(mode==IDLEMODE || mode == BUCKPAUSEMODE || mode == BOOSTPAUSEMODE || mode == ACPAUSEMODE) {
            hwConfig.setLedOff();
        }else if(mode==BUCKMODE || mode==BOOSTMODE || mode==ACMODE) {
            hwConfig.setLedOn();
        }else if(mode==BUCKINITMODE) {
            power.initAllBuck(VOLTAGE_MODE);
            mode = BUCKMODE;
        }else if(mode==BOOSTINITMODE) {
            power.initAllBoost();
            mode = BOOSTMODE;
        }else if(mode==ACINITMODE) {
            power.initLegMode(LEG1, PWMx1, VOLTAGE_MODE);
            power.initLegMode(LEG2, PWMx2, VOLTAGE_MODE);
            // power.setLegPhaseShift(LEG2, 180); 
            mode = ACMODE;
        }
        k_msleep(100);    
    }
}


void loop_control_task()
{
    time++;
    if(mode==IDLEMODE || mode==BUCKPAUSEMODE || mode==BOOSTPAUSEMODE || mode==ACPAUSEMODE) 
    {
        pwm_enable = false;
        power.stopAll(); // stop all legs
    }
    else if(mode==BUCKMODE || mode==BOOSTMODE) 
    {
        if(!pwm_enable) {//if not the power are not already started
            pwm_enable = true;

            //TURN Q5 AND Q6 ON - CONNECTS THE CAPACITORS
            gpio.resetPin(LEG1_CAPA_DGND); //the logic of the pins is reversed. Thus, if you set them, Q5 and Q6 are OFF
            gpio.resetPin(LEG2_CAPA_DGND);

            power.startAll(); // start all legs
        }

        //Sends the PWM to the switches
        power.setAllDutyCycle(duty_cycle); // set duty cycle for all legs
    }
    else if (mode==ACMODE)
    {
        if(!pwm_enable) {//if not the power are not already started
            pwm_enable = true;

            //TURN Q5 AND Q6 OFF - DISCONNECTS THE CAPACITORS
            gpio.setPin(LEG1_CAPA_DGND); //the logic of the pins is reversed. Thus, if you set them, Q5 and Q6 are OFF
            gpio.setPin(LEG2_CAPA_DGND);

            power.startAll(); // start all legs
        }
        duty_cycle_ac = 0.5 + 0.4 * sin(2*3.14159265359*frequency*time*control_task_period/1000000.0);
        power.setAllDutyCycle(duty_cycle_ac); // set duty cycle for all legs
    }
}

/**
 * This is the main function of this example
 * This function is generic and does not need editing.
 */

int main(void)
{
    setup_hardware();
    setup_software();

    return 0;
}
