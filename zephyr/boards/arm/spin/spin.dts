/*
 * Copyright (c) 2022-2023 OwnTech.
 *
 * SPDX-License-Identifier: LGLPV2.1
 */

/dts-v1/;

// SOC defintions
#include <st/g4/stm32g474Xe.dtsi>
#include <st/g4/stm32g474r(b-c-e)tx-pinctrl.dtsi>

// Board-related definitions
#include "spin_header.dtsi"
#include "pinctrl.dtsi"
#include "hrtim.dtsi"
#include "adc.dtsi"
#include "gpio.dtsi"


/ {
	model = "OwnTech Spin";
	compatible = "owntech,spin", "st,stm32g474re-nucleo";

	chosen {
		zephyr,console = &lpuart1;
		zephyr,shell-uart = &lpuart1;
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
		zephyr,can-primary = &can1;
	};

	leds {
		compatible = "gpio-leds";
		led: led_0 {
			gpios = <&gpioa 5 GPIO_ACTIVE_HIGH>;
		};
	};

	gpio_keys {
		compatible = "gpio-keys";
		btn: button_0 {
			label = "User button";
			gpios = <&gpioc 14 (GPIO_PULL_DOWN | GPIO_ACTIVE_HIGH)>;
		};
	};

	aliases {
		led0 = &led;
		sw0 = &btn;
	};
};

/**********/
/* Clocks */
/**********/

&clk_lsi {
	status = "okay";
};

&clk_hse {
	clock-frequency = <DT_FREQ_M(24)>;
	status = "okay";
};

&pll {
	div-m = <6>;
	mul-n = <85>;
	div-p = <7>;
	div-q = <2>;
	div-r = <2>;
	clocks = <&clk_hse>;
	status = "okay";
};

&rcc {
	clocks = <&pll>;
	clock-frequency = <DT_FREQ_M(170)>;
	ahb-prescaler = <1>;
	apb1-prescaler = <1>;
	apb2-prescaler = <1>;
};

&rtc {
	status = "okay";
};

/***********/
/* Storage */
/***********/

&flash0 {
	/*
	 * For more information, see:
	 * http://docs.zephyrproject.org/latest/guides/dts/index.html#flash-partitions
	 */
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		app_partition: partition@0 {
			label = "app";
			reg = <0x00000 0x79000>;
		};

		/* Set 4Kb of storage at the end of the 512Kb of flash */
		storage_partition: partition@79000 {
			label = "storage";
			reg = <0x79000 0x1000>;
		};
	};
};

/*****************/
/* Communication */
/*****************/

// UART

&usart1 {
	pinctrl-0 = <&usart1_tx_pb6 &usart1_rx_pb7>;
	pinctrl-names = "default";
	current-speed = <115200>;
	status = "okay";
};

&lpuart1 {
	pinctrl-0 = <&lpuart1_tx_pa2 &lpuart1_rx_pa3>;
	pinctrl-names = "default";
	current-speed = <115200>;
	status = "okay";
};

&usart3 {
	pinctrl-0 = < &usart3_tx_pc10 &usart3_rx_pc11 >;
	pinctrl-names = "default";
	current-speed = <10625000>;
	status = "okay";
};

// SPI

&spi3 {
	pinctrl-0 = <&spi3_nss_pa4 &spi3_sck_pc10 &spi3_miso_pb4 &spi3_mosi_pb5>;
	pinctrl-names = "default";
	status = "okay";
};

// CAN

&can1 {
	pinctrl-0 = <&fdcan1_rx_pb8 &fdcan1_tx_pb9>;
	pinctrl-names = "default";
	bus-speed = <500000>;
	sjw = <1>;
	sample-point = <875>;
	bus-speed-data = <500000>;
	sjw-data = <1>;
	sample-point-data = <875>;
	status = "okay";
};

/*********/
/* Timer */
/*********/

&timers4 {
	pinctrl-0 = <&tim4_etr_pb3 &tim4_ch1_pb6 &tim4_ch2_pb7>;
	pinctrl-names = "incremental_encoder";
	status = "okay";
};

&timers6 {
	status = "okay";
};

&timers7 {
	status = "okay";
};

/*******/
/* DMA */
/*******/

&dma1 {
	status = "okay";
};

&dmamux1 {
	status = "okay";
};

/*******/
/* ADC */
/*******/

&adc1 {
	pinctrl-0 = <&adc1_in6_pc0 &adc1_in7_pc1 &adc1_in8_pc2 &adc1_in9_pc3>;
	pinctrl-names = "default";
	status = "okay";
};

&adc2 {
	pinctrl-0 = <&adc2_in1_pa0 &adc2_in2_pa1 &adc2_in3_pa6 &adc2_in5_pc4>;
	pinctrl-names = "default";
	status = "okay";
};

&adc3 {
	pinctrl-0 = <&adc3_in1_pb1>;
	pinctrl-names = "default";
	status = "okay";
};

&adc4 {
	pinctrl-0 = <&adc4_in5_pb15>;
	pinctrl-names = "default";
	status = "okay";
};

&adc5 {
	pinctrl-0 = <&adc5_in1_pa8 &adc5_in2_pa9>;
	pinctrl-names = "default";
	status = "okay";
};

/*******/
/* DAC */
/*******/

&dac1 {
	status = "okay";
	pinctrl-0 = <&dac1_out1_pa4>;
	pinctrl-1 = <&dac1_out2_pa5>;
	pinctrl-names = "default", "secondary";
};

&dac2 {
	status = "okay";
	pinctrl-0 = <&dac2_out1_pa6>;
	pinctrl-names = "default";
};

&dac3 {
	status = "okay";
	pinctrl-0 = <&dac3_dummy>;
	pinctrl-names = "phantom";
};

/***********/
/* Various */
/***********/

&iwdg {
	status = "okay";
};

&rng {
	status = "okay";
};
